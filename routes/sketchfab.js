var express = require('express');
var router = express.Router();
var request = require('request');



// for getting image which opens up in the browser instead of getting downloaded automatically,ResponseContentDisposition not used here

router.get('/sketchfab', function (req, res) {

const email =req.query.adminemail;
//useremail is name of the model for the client who is the user
const client=req.query.useremail;
console.log(req.query.client);

var options = {
  url: 'https://api.sketchfab.com/v3/search?type=models&q='+client+'&user='+email,
  headers: {
      'Authorization': 'Token 4ddc9d30bf2541e6ab594d0078023a5a'}
};
 
function callback(error, response, body) {
  
    if(error){
     console.log(error);
    }
  if (!error && response.statusCode == 200) {
    var info = JSON.parse(body);

    res.send(info);
    
  }
}
 
request(options, callback);

});

module.exports = router;
