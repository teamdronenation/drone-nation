var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var mongoose = require('mongoose');

var appRoutes = require('./routes/app');
// var messageRoutes = require('./routes/messages');
var userRoutes = require('./routes/user');
var sfRoutes = require('./routes/sketchfab');

// var uploads3Routes=require('./routes/uploads3routes');

var gets3=require('./routes/gets3');

var app = express();
mongoose.connect('localhost:27017/node-angular');

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'hbs');


// var bodyParser = require('body-parser');

// uncomment after placing your favicon in /public
app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(logger('dev'));
// app.use(bodyParser.json());
app.use(bodyParser.json({limit: '100mb'}));
app.use(bodyParser.urlencoded({limit: '100mb', extended: true}));
// app.use(bodyParser.urlencoded({extended: false}));


app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use(function (req, res, next) {
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept');
    res.setHeader('Access-Control-Allow-Methods', 'POST, GET, PATCH, DELETE, OPTIONS');
    next();
});

// app.use('/message', messageRoutes);
app.use('/s3',gets3);
app.use('/user', userRoutes);
app.use('/', appRoutes);
app.use('/s', sfRoutes);




// catch 404 and forward to error handler
app.use(function (req, res, next) {
  
    console.log("error occures index file");
    return res.render('index');
});


module.exports = app;
