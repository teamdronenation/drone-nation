import { Component } from "@angular/core";
import { FormGroup, FormControl, Validators } from "@angular/forms";
import { Router } from "@angular/router";

import { User } from "../user.model";
import { AuthService } from "../auth.service";

@Component({
    selector: 'app-signin',
    templateUrl: './signin.component.html',
    styleUrls: ['./signin.component.css'],
})
export class SigninComponent {
    myForm: FormGroup;

    constructor(private authService: AuthService, private router: Router) {}

    onSubmit() {

        console.log("sign in button clicked");
        const user = new User(this.myForm.value.email, this.myForm.value.password);
        
        this.authService.signin(user)
            .subscribe(
                data => {
                    console.log(data);
                    localStorage.setItem('token', data.token);
                    localStorage.setItem('userId', data.userId);
                    localStorage.setItem('email', data.email);
                    //added by me
                    console.log("ssss");
                    // console.log(this.myForm.value.email);
                    // localStorage.setItem('email', this.myForm.value.email);

                     console.log("signin button clicked and no error");
                     //till here
                    this.router.navigateByUrl('/home');
                },
                error => console.error(error)
            );
        this.myForm.reset();
    }

    ngOnInit() {
            console.log("signin component init");
        this.myForm = new FormGroup({
            email: new FormControl(null, [
                Validators.required,
                Validators.pattern("[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?")
            ]),
            password: new FormControl(null, Validators.required)
        });
    }
}