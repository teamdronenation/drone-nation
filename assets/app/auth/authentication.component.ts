import { Component ,OnInit} from "@angular/core";
import { AuthService } from "./auth.service";
import { Router,ActivatedRoute  } from "@angular/router";
@Component({
    selector: 'app-authentication',
    templateUrl: './authentication.component.html',
    styleUrls: ['./authentication.component.css']
})
export class AuthenticationComponent implements OnInit{


    constructor(private authService: AuthService,private router: Router,private activatedRoute : ActivatedRoute) {}
     ngOnInit(){


        var signupButton = document.getElementById('signup-button');
        var loginButton  = document.getElementById('login-button');
        var userForms    = document.getElementById('user_options-forms');



this.activatedRoute.url.subscribe(url =>{

if(this.router.url==="/auth/signup"){
console.log("signup");

  userForms.classList.remove("bounceRight");
        userForms.classList.add("bounceLeft");

}else
{
      userForms.classList.remove("bounceLeft");
    userForms.classList.add("bounceRight");
console.log("signin");
}
console.log(url);
    });



signupButton.addEventListener(
        "click",
        () => {
            //   console.log(this.router.url); /// this will give you current url
        userForms.classList.remove("bounceRight");
        userForms.classList.add("bounceLeft");
        },
  false
);

/**
 * Add event listener to the "Login" button
 */
loginButton.addEventListener(
  "click",
  () => {
        // console.log(this.router.url); /// this will give you current url
    userForms.classList.remove("bounceLeft");
    userForms.classList.add("bounceRight");
  },
  false
);

      console.log("auth component init");


     }
    isLoggedIn() {
            console.log("auth comp islogged in method");
        return this.authService.isLoggedIn();
    }
}