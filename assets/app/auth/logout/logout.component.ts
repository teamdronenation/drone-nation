import { Component } from "@angular/core";
import { Router } from "@angular/router";

import { AuthService } from "../auth.service";
// import * as logo from '../../../public/images/vv.png';

@Component({
    selector: 'app-logout',
      templateUrl: './logout.component.html',
        styleUrls: ['./logout.component.css'],
})
export class LogoutComponent {
    constructor(private authService: AuthService, private router: Router) {}

    onLogout() {
        this.authService.logout();
        this.router.navigate(['/auth', 'signin']);
    }
}