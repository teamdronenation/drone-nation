import { Component, OnInit } from "@angular/core";
import { FormGroup, FormControl, Validators } from "@angular/forms";

import { AuthService } from "../auth.service";
import { User } from "../user.model";
import { Router } from "@angular/router";
declare var jquery:any;

declare var $ :any;

@Component({
    selector: 'app-signup',
    templateUrl: './signup.component.html',
      styleUrls: ['./signup.component.css'],
})
export class SignupComponent implements OnInit {
    myForm: FormGroup;

    constructor(private authService: AuthService, private router: Router) {
        
    }

    onSubmit() {
        
      
        const user = new User(
            this.myForm.value.email,
            this.myForm.value.password,
            this.myForm.value.firstName,
            this.myForm.value.lastName
        );
        this.authService.signup(user)
            .subscribe(
                data => {
                    this.router.navigate(['/auth/signin']);
                  // this.router.navigateByUrl('/home');
                     console.log(data);
                },
                error => console.error(error)

            );
        this.myForm.reset();
    }

    ngOnInit() {

		// $(window).load(function(){
		// $(".effect-1").val("");
		
		// $(".input-effect input").focusout(function(){
		// 	if($(this).val() != ""){
		// 		$(this).addClass("has-content");
		// 	}else{
		// 		$(this).removeClass("has-content");
		// 	}
		// })












      console.log("signup component init");
            this.myForm = new FormGroup({
            firstName: new FormControl(null, Validators.required),
            lastName: new FormControl(null, Validators.required),
            email: new FormControl(null, [
                Validators.required,
                Validators.pattern("[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?")
            ]),
            password: new FormControl(null, Validators.required)
        });
    }
}