import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";
import { AdminService } from "./admin.service";
import {  FileUploader } from 'ng2-file-upload/ng2-file-upload';
const URL = 'http://localhost:3000/s3/aa';

@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.css']
})

export class AdminComponent implements OnInit {

public uploader:FileUploader = new FileUploader({url: URL, disableMultipart:true});


constructor(private adminService: AdminService, private router: Router) {
}

ngOnInit(){

 
// var fileName = 'Demo.zip';
// var filePath = './' + fileName;
// var fileKey = fileName;
// // var buffer = fs.readFileSync('./' + filePath);


// this.adminService.uploadimage()
//  this.uploader.onAfterAddingFile = (file)=> { file.withCredentials = false; };
//        //overide the onCompleteItem property of the uploader so we are 
//        //able to deal with the server response.
//        this.uploader.onCompleteItem = (item:any, response:any, status:any, headers:any) => {
//             console.log("ImageUpload:uploaded:", item, status, response);
//         };

         //override the onAfterAddingfile property of the uploader so it doesn't authenticate with //credentials.
       this.uploader.onAfterAddingFile = (file)=> { file.withCredentials = false; };
       //overide the onCompleteItem property of the uploader so we are 
       //able to deal with the server response.
       this.uploader.onCompleteItem = (item:any, response:any, status:any, headers:any) => {
            console.log("ImageUpload:uploaded:", item, status, response);
        };
}

}