
const base:string='http://localhost:3000';


export const USER: string = base+'/user';

export const USER_SIGNIN: string = base+'/user/signin';

export const S3_KML: string = base+'/s3/kml';

export const S3_OVERLAYIMAGE: string =  base+'/s3/overlayimage';

export const S3_DOWNLOADFROMS3: string = base+'/s3/downloadfroms3';


