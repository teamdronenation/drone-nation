import { Routes, RouterModule } from "@angular/router";


import { HomeComponent } from "./home/home.component";
import { AuthenticationComponent } from "./auth/authentication.component";
import { AUTH_ROUTES } from "./auth/auth.routes";
import { HOME_ROUTES } from "./home/home.routes";

const APP_ROUTES: Routes = [
    { path: '', redirectTo: '/home', pathMatch: 'full' },
   
    { path: 'auth', component: AuthenticationComponent, children: AUTH_ROUTES },
    { path: 'home', component: HomeComponent ,children: HOME_ROUTES },
];

export const routing = RouterModule.forRoot(APP_ROUTES);
