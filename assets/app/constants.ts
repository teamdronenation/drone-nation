// FILES FOR SPECIFIC USERS



//two d output
export const IMAGE_OVERLAY: string = '2doverlay.png';

//kml file of two d output
export const KML_FILE: string = '2doverlay.kml';

//three d output
export const THREED_MODEL: string = 'model.zip';

//vegetation output
export const VEGETATION: string = 'Vegetation.jpg';

//contour output
export const CONTOUR: string = 'Contour.jpg';

//digital terrain output
export const DIGITAL: string = 'DigitalTerrain.jpg';

//elevation output
export const ELEVATION: string = 'Elevation.jpg';






//PUBLIC FILES

// Icons

// two D  icon
export const TWOD_ICON: string ='https://s3.ap-south-1.amazonaws.com/dronenation/drone-nation/public/2d.png';

// three D icon
export const THREE_ICON: string ='https://s3.ap-south-1.amazonaws.com/dronenation/drone-nation/public/3d.png';

// vegetation icon
export const VEGETATION_ICON: string ='https://s3.ap-south-1.amazonaws.com/dronenation/drone-nation/public/Vegetation.png';

// contour icon
export const CONTOUR_ICON: string ='https://s3.ap-south-1.amazonaws.com/dronenation/drone-nation/public/Contour.png';

// digital terrain icon
export const DIGITAL_ICON: string ='https://s3.ap-south-1.amazonaws.com/dronenation/drone-nation/public/DigitalTerrain.png';

// elevation icon
export const ELEVATION_ICON: string ='https://s3.ap-south-1.amazonaws.com/dronenation/drone-nation/public/Elevation.png';



// SKETCHFAB DETAILS

//username of the account which uploads all the models for the clients
export  const SKETCHFAB_ADMIN:string='anushasingh996';




 






