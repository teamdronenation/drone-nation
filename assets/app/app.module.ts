import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { HttpModule } from "@angular/http";
import { AppComponent } from "./app.component";
import { AuthenticationComponent } from "./auth/authentication.component";
// import { HeaderComponent } from "./header.component";
import { routing } from "./app.routing";
import { LogoutComponent } from "./auth/logout/logout.component";
import { SignupComponent } from "./auth/signup/signup.component";
import { SigninComponent } from "./auth/signin/signin.component";
import { AuthService } from "./auth/auth.service";
import { HomeComponent } from "./home/home.component";
import { ThreeDComponent } from "./home/ThreeD/threeD.component";
import { TwoDComponent } from "./home/TwoD/twoD.component";
import { DataService } from './home/services/data.service';
import { AdminService } from './admin/admin.service';
import { AdminComponent } from "./admin/admin.component";
import { FileSelectDirective } from 'ng2-file-upload';
import { HttpClientModule } from '@angular/common/http';
import { LoadingModule } from 'ngx-loading';
import { VegetationComponent } from "./home/vegetation/vegetation.component";
import { ContourComponent } from "./home/contour/contour.component";
import { DigitalTerrainComponent } from "./home/digitalTerrain/digitalterrain.component";
import { ElevationComponent } from "./home/elevation/elevation.component";
import { NgxGalleryModule } from 'ngx-gallery';


@NgModule({
    declarations: [
        AppComponent,
     
        AuthenticationComponent,
        // HeaderComponent,
        LogoutComponent,
        SignupComponent,
        SigninComponent,
        HomeComponent,
        ThreeDComponent,
        TwoDComponent,
        AdminComponent,
        FileSelectDirective,
     
         VegetationComponent,
         ContourComponent,
         DigitalTerrainComponent,
         ElevationComponent,
        
        
    ],
    imports: [
        BrowserModule,
        FormsModule,
        routing,
        ReactiveFormsModule,
        HttpModule,
        HttpClientModule,
    
         LoadingModule,
          NgxGalleryModule,
        
    ],
    providers: [AuthService,DataService,AdminService],
    bootstrap: [AppComponent]
})
export class AppModule {

}