import { Component, OnInit } from '@angular/core';
import * as MTLLoader  from 'three-mtl-loader';
import OrbitControls   from 'orbit-controls-es6';
import * as OBJLoader  from 'three-obj-loader';
import { DataService } from '../services/data.service';


@Component({
  selector: 'app-threeD',
  templateUrl: './threeD.component.html',
  styleUrls: ['./threeD.component.css']
})
export class ThreeDComponent implements OnInit {
constructor(private dataService:DataService) {}
public loading2 = false;

public noerror=true;
public x;


ngOnInit() {
    this.loading2=true;
    // this.spinnerService.show();
var email=localStorage.getItem('email'); //gets the email id of the current signed in user

var firstpartemail=email.substring(0, email.indexOf("@")); //assigns first part of email like, imuser@gmail.com to only  imuser    
this.dataService.get3dpath(firstpartemail).subscribe(

(response)=>{
   console.log(response);
      this.loading2=false;
// this.spinnerService.hide();
   var data= response;
//    console.log(response.json());
//  var data= response;
//    var data=response["url"];
   var iframe = document.getElementById( 'api-frame' );  //remove DOM manipulation
//    this.x=response["results"];
 if( response["results"].length!=0){
   var urlid = data["results"][0]["uid"];  


    // By default, the latest version of the viewer API will be used.

    var client = new Sketchfab( iframe );

    // Alternatively, to request a specific version -->
    // var client = new Sketchfab( '1.0.0', iframe );

    client.init( urlid, {
        success: function onSuccess( api ){
         
            api.start();
            api.addEventListener( 'viewerready', function() {
                                                                     
        });
        },

        error: function onError() {
     
            this.noerror=false;
        },
        autostart: 1,
    
    });
 
 }else
 {
     this.noerror=false;
 }
},
(error)=>{

console.log(error);
this.noerror=false;

});  }
}