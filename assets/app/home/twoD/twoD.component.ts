import { Component, OnInit } from "@angular/core";
import { DataService } from '../services/data.service';
declare var google: any;
import * as parse from 'xml-parser';
import 'rxjs/add/operator/mergeMap';

import * as constants from '../../constants';


@Component({
  selector: 'app-twoD',
  templateUrl: './twoD.component.html',
  styleUrls: ['./twoD.component.css']
})
export class TwoDComponent implements OnInit {
  constructor(private dataService:DataService) {}
public loading = false;
public noerror=true;


ngOnInit(){
  
 this.loading=true;
// this.spinnerService.show();
  this.getkml(); 

  
}



getkml(){

var email=localStorage.getItem('email'); //gets the email id of the current signed in user

var firstpartemail=email.substring(0, email.indexOf("@")); //assigns first part of email like, imuser@gmail.com to only  imuser    
var filename=constants.KML_FILE;

  this.dataService.getkmlfile(firstpartemail,filename).subscribe(              //gets kml file (response's body contains the text of the kml file)
  (response)=>{
                 
              let coordinatearray=this.parsekml(response["text"]); //parses the kml file and stores the coordinates in an array
              this.initMap(coordinatearray);                        //initializes the map

              },
                      
  (error)=>{

      console.log(error);
      this.noerror=false;


    }

)};









parsekml(response){

/*example kml file passed correctly with the help of following function

<?xml version="1.0" encoding="UTF-8"?>
<kml xmlns="http://earth.google.com/kml/2.2">
    <Document>
        <GroundOverlay>
            <Icon>
                <href>Demo_Orthomosaic_export_SatDec23.jpg</href>
            </Icon>
            <LatLonBox>
                <north>13.0615027778</north>
                <south>13.060375</south>
                <east>77.3244805556</east>
                <west>77.3231083333</west>
            </LatLonBox>
            <name>Demo_Orthomosaic_export_SatDec23.jpg</name>
            <drawOrder>1</drawOrder>
        </GroundOverlay>
    </Document>
</kml>

*/

var xml = response;
var inspect = require('util').inspect;
var obj = parse(xml);


//can be made less specific when accessing array index

for (var prop in obj) {
    if (obj.hasOwnProperty(prop)&&prop=="root") {
         var obj2=obj[prop];
     
         for (var prop in obj2) {
            if (obj2.hasOwnProperty(prop)&&prop=="name"&&obj2[prop]=="kml") {

                prop="children";
                var obj2=obj2[prop][0];
               
                for (var prop in obj2) {
                  
                if (obj2.hasOwnProperty(prop)&&prop=="name"&&obj2[prop]=="Document") {
               
                prop="children";
                var obj2=obj2[prop][0];
                
                   for (var prop in obj2) {
                if (obj2.hasOwnProperty(prop)&&prop=="name"&&obj2[prop]=="GroundOverlay") {
                

                 prop="children";
               
                 // at this point json has name prop as latlang box
                
                 var north=obj2[prop][1][prop][0]["content"];
                 var south=obj2[prop][1][prop][1]["content"];
                 var east=obj2[prop][1][prop][2]["content"];
                 var west=obj2[prop][1][prop][3]["content"];

                 var arr=[north,south,east,west];

                
                
                } }
           } }
        
     } }
} }

//  example array returned --> ["13.0615027778", "13.060375", "77.3244805556", "77.3231083333"]

return arr;

}


initMap(coordinatearray){


var imageOverlay; 




var imageBounds = {

        north:  parseFloat(coordinatearray[0]),                 //parsefloat is used because arr contains strings not numbers
        south:  parseFloat(coordinatearray[1]),
        east:   parseFloat(coordinatearray[2]),
        west:   parseFloat(coordinatearray[3]) 
         
        };


var imagename=constants.IMAGE_OVERLAY;
var email=localStorage.getItem('email'); //gets the email id of the current signed in user

var firstpartemail=email.substring(0, email.indexOf("@")); //assigns first part of email like, imuser@gmail.com to only  imuser    
this.dataService.getImage(firstpartemail,imagename).subscribe(                      

            (response)=>{
               
            var img = new Image();
            var img_url = response["url"];
            img.src= img_url;
            
            img.addEventListener('load', ()=> { 
                  
                let c_lat = parseFloat(coordinatearray[0]);
                let c_long = parseFloat(coordinatearray[2]);
 
                 imageOverlay = new google.maps.GroundOverlay(img_url,imageBounds); 
                 let map = new google.maps.Map(document.getElementById('map'), { 
    
                          zoom: 17,                                               //need to make this general
                          center: {lat:c_lat, lng: c_long} ,              //need to make this general
                          mapTypeId: 'satellite',                                 //use this if you want satellite mode ,by default normal map mode 
                          mapTypeControlOptions: {
                          mapTypeIds: [google.maps.MapTypeId.ROADMAP,google.maps.MapTypeId.SATELLITE],
                          disableDefaultUI: false, // a way to quickly hide all controls
                          }, 
                     });
     //response["_body"] contains the url of the image to be overlayed
   
               imageOverlay.setMap(map);
             this.loading=false;
             
                 });
      
            },
            (error)=>
            {console.log(error);
             this.noerror=false;
        
               }
             
            );

   }

 

}