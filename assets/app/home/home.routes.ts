import { Routes } from "@angular/router";
import { ThreeDComponent } from "./ThreeD/threeD.component";
import { TwoDComponent } from "./TwoD/twoD.component";

import { VegetationComponent } from "./vegetation/vegetation.component";
import { ContourComponent } from "./contour/contour.component";
import { DigitalTerrainComponent } from "./digitalTerrain/digitalterrain.component";
import { ElevationComponent } from "./elevation/elevation.component";



export const HOME_ROUTES: Routes = [
    { path: 'twoD', component: TwoDComponent },
    { path: 'threeD', component: ThreeDComponent },

    { path: 'vegetation', component:VegetationComponent },
    { path: 'contour', component: ContourComponent },
    { path: 'elevation', component: ElevationComponent },
    { path: 'digital', component: DigitalTerrainComponent }
  

];