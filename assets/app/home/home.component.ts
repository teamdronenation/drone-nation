import { Component, OnInit } from "@angular/core";
// import { FormGroup, FormControl, Validators } from "@angular/forms";
import { Router } from "@angular/router";
import { AuthService } from "../auth/auth.service";

import { DataService } from "./services/data.service";

declare var jquery:any;
declare var $ :any;
import * as b64toBlob from 'b64-to-blob';

import * as constants from '../constants';



@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
public ifselected=true;
public downloadlink="";
public  selectedIndex= '';

public twod_icon="";
public threed_icon="";
public contour_icon="";
public vegetation_icon="";
public digital_icon="";
public elevation_icon="";

constructor(private authService: AuthService,private router: Router,private dataservice :DataService) {


if (authService.isLoggedIn()) {
          
  
    }else
    {
        this.router.navigateByUrl('/auth/signup');
    }
  }






  ngOnInit() {
    this.selectedIndex='0';
    
this.router.navigate(['/home/twoD']);
//    document.getElementById('logo').setAttribute('src',
//        logo );


this.twod_icon=constants.TWOD_ICON;
this.threed_icon=constants.THREE_ICON;
this.contour_icon=constants.CONTOUR_ICON;
this.elevation_icon=constants.ELEVATION_ICON;
this.digital_icon=constants.DIGITAL_ICON;
this.vegetation_icon=constants.VEGETATION_ICON;




//     $('document').ready(function() { 
//   $('.icon').click(function(){
//     console.log("clicked");
//     var icon = $('i.icon');
//     if($(this).hasClass('closed')){
//        console.log("has class closed");
//       $('.move').animate({
//         marginLeft: '0px'
//       }, 500);
//       $(this).removeClass('closed');
//       $(this).addClass('open');
//       icon.addClass('fa-chevron-left');
//       icon.removeClass('fa-chevron-right');   


//     }
//     else{
//       $('.move').animate({
//         marginLeft: '-341px'
//       }, 500);
      
//       $(this).removeClass('open');
//       $(this).addClass('closed');
//       icon.addClass('fa-chevron-right');
//       icon.removeClass('fa-chevron-left');    
//     }
//   });
// });




	

}


// following methods for loading different components -->

// 2D output button
//loads twoD component in <router-outlet></router-outlet>
twoD(index){
console.log(index);
 this.selectedIndex = index;

this.router.navigate(['/home/twoD']);
}

// 3D output button
//loads threeD component in <router-outlet></router-outlet>
threeD(index){
   this.selectedIndex = index;
this.router.navigate(['/home/threeD']);
}  


// Elevation output button
//loads Elevation component in <router-outlet></router-outlet>
elevation(index){
   this.selectedIndex = index;
this.router.navigate(['/home/elevation']);
}

// Contour output button
//loads contour component in <router-outlet></router-outlet>
contour(index){
   this.selectedIndex = index;
this.router.navigate(['/home/contour']);
}


// Digital output button
//loads digital component in <router-outlet></router-outlet>
digital(index){
   this.selectedIndex = index;
this.router.navigate(['/home/digital']);

}


// vegetation output button
//loads vegetation component in <router-outlet></router-outlet>
vegetation(index){
   this.selectedIndex = index;

this.router.navigate(['/home/vegetation']);
}



//  following methods for downloading different file -->
  

downloadImageoverlay(){

var filename=constants.IMAGE_OVERLAY;

this.downloadfile(filename);

}



downloadvegetation(){

var filename=constants.VEGETATION;

this.downloadfile(filename);

}

downloadcontour(){

var filename=constants.CONTOUR;

this.downloadfile(filename);

}


downloaddigital(){

var filename=constants.DIGITAL;

this.downloadfile(filename);

}


downloadelevation(){

var filename=constants.ELEVATION;

this.downloadfile(filename);

}

downloadthreed(){

var filename=constants.THREED_MODEL;

this.downloadfile(filename);

}





// general method for downloading

downloadfile(filename){  // parameter filename should include extension , example --> image.jpg
 
var email=localStorage.getItem('email'); //gets the email id of the current signed in user

var firstpartemail=email.substring(0, email.indexOf("@")); //assigns first part of email like, imuser@gmail.com to only  imuser


this.dataservice.getdownloadpath(firstpartemail,filename).subscribe(
      (response)=>{
                  var url=response["url"];
                  location.href=url;
                },
       (error)=>console.log(error));

    }














}
