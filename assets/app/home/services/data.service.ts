import { Injectable } from "@angular/core";
import { Http, Headers, Response ,RequestOptions} from "@angular/http";
import 'rxjs/Rx';
import { Observable } from "rxjs";
import { HttpParams } from '@angular/common/http';
import {HttpClient} from '@angular/common/http';
import 'rxjs/add/operator/mergeMap';

import * as constants from '../../constants'
// import  sketchfabdetails from '../../../api.urlconstants';

import * as url from '../../api.urlconstants';
@Injectable()
export class DataService {

constructor(private http: Http,private httpclient: HttpClient) {}



//returns kml file which is needed to overlay the image correctly over the map 
//the text of the file is returned
getkmlfile(useremail,filename){    
    
    
    let params = new HttpParams().append("email",useremail); 
    params=params.append("filename",filename); 

    //url.s3_kml will return /s3/kml routes's full url--> in development mode it is http://localhost:3000/user
    return this.httpclient.get(url.S3_KML, { params: params}).map((response: Response) => response);

}



// for getting image Url which is to be overlayed on the map
getImage(useremail,filename){ 

   let params = new HttpParams().append("email",useremail); 
   params=params.append("filename",filename); 
   
    //url.s3_kml will return /s3/overlayimage routes's full url--> in development mode it is http://localhost:3000/s3/overlayimage
   return this.httpclient.get(url.S3_OVERLAYIMAGE, { params: params}).map((response: Response) => response) ;

   // response's body contains the URL where image is hosted 

}


get3dpath(useremail){

//email of the admin who uploads all the models for different clients

let params = new HttpParams().append("adminemail",constants.SKETCHFAB_ADMIN); 

//email of the client(signed in user)

params=params.append("useremail",useremail); 

    //url.s3_kml will return /s3/downloadfroms3 routes's full url--> in development mode it is http://localhost:3000/s3/downloadfroms3
return this.httpclient.get("http://localhost:3000/s/sketchfab", { params: params}) .map((response: Response) => response);







}


// for getting URLs for different files for the purpose of downloading
// filename must contain extension also. example --> image.jpg
getdownloadpath(useremail,filename){
    
let params = new HttpParams().append("email",useremail); 

params=params.append("filename",filename); 

    //url.s3_kml will return /s3/downloadfroms3 routes's full url--> in development mode it is http://localhost:3000/s3/downloadfroms3
return this.httpclient.get(url.S3_DOWNLOADFROMS3, { params: params}) .map((response: Response) => response);
  
 }


 
}