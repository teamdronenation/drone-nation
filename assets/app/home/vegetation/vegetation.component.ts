import { Component, OnInit } from "@angular/core";
import { DataService } from '../services/data.service';

import * as constants from '../../constants';
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';
import { NgxGalleryOptions, NgxGalleryImage, NgxGalleryAnimation } from 'ngx-gallery';
@Component({
  selector: 'app-vegetation',
  templateUrl: './vegetation.component.html',
  styleUrls: ['./vegetation.component.css']
})
export class VegetationComponent implements OnInit {
public url;
  galleryOptions: NgxGalleryOptions[];
    galleryImages: NgxGalleryImage[];
public loading = false;
public noerror=true;

  constructor(private dataService:DataService) {}


ngOnInit(){

 this.loading=true;

var imagename=constants.VEGETATION;

var email=localStorage.getItem('email'); //gets the email id of the current signed in user

var firstpartemail=email.substring(0, email.indexOf("@")); //assigns first part of email like, imuser@gmail.com to only imuser 
 this.galleryOptions = [
     
            {
                width: '600px',
                height: '400px',
                thumbnailsColumns: 0,
                imageAnimation: NgxGalleryAnimation.Slide
            },
            // max-width 800
            {
                breakpoint: 800,
             
                imagePercent: 80,
                thumbnailsPercent: 20,
                thumbnailsMargin: 20,
                thumbnailMargin: 20
            },
            // max-width 400
            {
                breakpoint: 400,
                preview: false
            },
            { imageArrows: false,
             thumbnailsArrows: false ,
             thumbnails: false ,
             arrowPrevIcon :null,
            arrowNextIcon :null,
            previewZoom :true,

            }
        ];
 
   

this.dataService.getImage(firstpartemail,imagename).subscribe(
(response)=>{
this.url=response["url"];
      var img = new Image();
    var img_url = response["url"];
         img.src= img_url;
            img.addEventListener('error', ()=> { 
             this.noerror=false;

           } );
            img.addEventListener('load', ()=> { 
     this.galleryImages = [
            {
                    small: null,
                    medium: response["url"],
                    big: response["url"]
            }
        ];
        this.loading=false;
    
});
    },

(error)=>{console.log(error);
this.noerror=false;

}
);

}

}

