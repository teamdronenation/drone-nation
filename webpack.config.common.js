var webpack = require('webpack');

// var urlloader = require('url-loader');

module.exports = {
    entry: {
        'app': './assets/app/main.ts'
    },

    resolve: {
        extensions: ['.js', '.ts']
    },

    module: {
        rules: [
            {
                test: /\.html$/,
                use: [{ loader: 'html-loader' }]
            },
            {
                test: /\.css$/,
                use: [{ loader: 'raw-loader' }]
            },
            {   test: /\.(png|jpg|gif)$/,
                use: [{
                loader: 'url-loader',
                options: {
                         limit: 8192
                          }
                  }]
            }, {
           test: /\.(png|jpg|gif)$/,
           use: [
           {
            loader: 'file-loader',
            options: { emitFile: false }
          }]}
        ],
        exprContextCritical: false

    }
};